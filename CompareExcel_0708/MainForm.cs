﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SpreadsheetLight;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Diagnostics;
using CompareExcel_0708.enums;

namespace CompareExcel_0708
{
    public partial class MainForm : Form
    {
        string firstPath, secondPath;
        public MainForm()
        {
            InitializeComponent();

        }

        private void btn_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx";
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
            
            DataGridView dgv = null;

            switch ((sender as Button).Name)
            {
                case "btn_First":
                    {
                        firstPath = openFileDialog1.FileName;
                        Filltheview(openFileDialog1.FileName, dgvOrder.First);
                        dgv = dtg_First;
                        break;
                    }
                case "btn_Second":
                    {
                        secondPath = openFileDialog1.FileName;
                        Filltheview(openFileDialog1.FileName, dgvOrder.Second);
                        dgv = dtg_Second;
                        break;
                    }
            }

            dgv.ClearSelection();
            foreach (DataGridViewColumn c in dgv.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;
            dgv.SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
            dgv.ReadOnly = true;
        }

        private void btn_Compare_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                CompareSheets(saveFileDialog1.FileName);
        }

        private void cmb_secondSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SLDocument doc = new SLDocument(secondPath);
                doc.SelectWorksheet(cmb_secondSheet.SelectedItem.ToString());

                DataTable dt = BuildDatatable(doc);
                dtg_Second.DataSource = null;
                dtg_Second.SelectionMode = DataGridViewSelectionMode.CellSelect;
                dtg_Second.DataSource = dt;
                 
                foreach (DataGridViewColumn c in dtg_Second.Columns)
                    c.SortMode = DataGridViewColumnSortMode.NotSortable;

                dtg_Second.SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
                dtg_Second.ReadOnly = true;

            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void cmb_firstSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SLDocument doc = new SLDocument(firstPath);
                doc.SelectWorksheet(cmb_firstSheet.SelectedItem.ToString());

                DataTable dt = BuildDatatable(doc);

                dtg_First.DataSource = null;
                dtg_First.SelectionMode = DataGridViewSelectionMode.CellSelect;
                dtg_First.DataSource = dt;
                foreach (DataGridViewColumn c in dtg_First.Columns)
                    c.SortMode = DataGridViewColumnSortMode.NotSortable;
                dtg_First.SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
                dtg_First.ReadOnly = true;

            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private void Filltheview(string fileName, dgvOrder choice)
        {

            try
            {
                SLDocument doc = new SLDocument(fileName);
                var SheetNames = doc.GetSheetNames();
                var chosen = choice == dgvOrder.First ? cmb_firstSheet : cmb_secondSheet;

                foreach (var item in SheetNames)
                {
                    chosen.Items.Add(item);
                }

                chosen.SelectedIndex = 0;
                doc.SelectWorksheet(chosen.SelectedItem.ToString());

                var dgv = choice == dgvOrder.First ? dtg_First : dtg_Second;

                DataTable dt = BuildDatatable(doc);

                dgv.DataSource = dt;
                dgv.Refresh();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
        }

        private DataTable BuildDatatable(SLDocument doc)
        {
            var dt = new DataTable();
            var rows = new List<List<string>>();

            for (int i = 1; i <= doc.GetWorksheetStatistics().EndRowIndex; i++)
            {
                var row = new List<string>();
                for (int j = 1; j <= doc.GetWorksheetStatistics().EndColumnIndex; j++)
                    row.Add(doc.GetCellValueAsString(i, j).ToString());
                rows.Add(row);
            }
            if (rows.Any())
            {
                foreach (var columnName in rows.First())
                {
                    if (dt.Columns.Contains(columnName))
                        dt.Columns.Add($"{columnName}({(dt.Columns.Cast<DataColumn>().Count(col => col.ColumnName == columnName || (col.ColumnName.LastIndexOf(columnName) == 0 && col.ColumnName.Contains('('))).ToString())})");
                    else dt.Columns.Add(columnName);
                }

                //rows.First().ToList().ForEach(columnName => dt.Columns.Add(dt.Columns.Contains(columnName) ?
                //       $"{columnName}({(dt.Columns.Cast<DataColumn>().Count(col => col.ColumnName == columnName || (col.ColumnName.LastIndexOf(columnName) == 0 && col.ColumnName.Contains('('))).ToString())})" :
                //       columnName));

                rows.Skip(1).ToList().ForEach(row => dt.Rows.Add(row.ToArray()));
            }

            return dt;
        }

        private void CompareSheets(string fileName)
        {
            try
            {
                int firstId = dtg_First.CurrentCell.ColumnIndex + 1;
                int secondId = dtg_Second.CurrentCell.ColumnIndex + 1;
                var firstSheet = cmb_firstSheet.SelectedItem.ToString();
                var secondSheet = cmb_firstSheet.SelectedItem.ToString();
                List<string> firstList = new List<string>();
                List<string> secondList = new List<string>();
                List<string> firstHeader = new List<string>();
                List<string> secondHeader = new List<string>();
                List<string> Empty = new List<string>() { "", "" };
                string id = "", idLast = "";
                List<String> row;
                var firstDoc = new SLDocument(firstPath, firstSheet);
                var secondDoc = new SLDocument(secondPath, secondSheet);
                Dictionary<string, List<List<string>>> firstDic = new Dictionary<string, List<List<string>>>();
                Dictionary<string, List<List<string>>> secondDic = new Dictionary<string, List<List<string>>>();
                var list = new List<List<string>>();

                // Filling the dictionary
                for (int i = 2; i <= firstDoc.GetWorksheetStatistics().EndRowIndex; i++)
                {
                    id = firstDoc.GetCellValueAsString(i, firstId).Replace("-", "").Replace(" ", "").ToLower();
                    firstList.Add(id);
                    row = new List<string>();
                    for (int j = 1; j <= firstDoc.GetWorksheetStatistics().EndColumnIndex; j++)
                        row.Add(firstDoc.GetCellValueAsString(i, j));
                    if (idLast == id)
                    {
                        list.Add(row);
                        firstDic.Remove(id);
                        firstDic.Add(id, list);
                    }
                    else
                    {
                        list = new List<List<string>>();
                        list.Add(row);
                        firstDic.Add(id, list);
                    }

                    idLast = id;
                    //firstCol.Add(new KeyValuePair<string, List<string>>(id,row));
                }
                id = "";
                idLast = "";
                for (int i = 2; i <= secondDoc.GetWorksheetStatistics().EndRowIndex; i++)
                {
                    id = secondDoc.GetCellValueAsString(i, secondId).Replace("-", "").Replace(" ", "").ToLower();
                    secondList.Add(id);
                    row = new List<string>();
                    for (int j = 1; j <= secondDoc.GetWorksheetStatistics().EndColumnIndex; j++)
                        row.Add(secondDoc.GetCellValueAsString(i, j));
                    if (idLast == id)
                    {
                        list.Add(row);
                        secondDic.Remove(id);
                        secondDic.Add(id, list);
                    }
                    else
                    {
                        list = new List<List<string>>();
                        list.Add(row);
                        secondDic.Add(id, list);
                    }

                    idLast = id;
                    //secondCol.Add(new KeyValuePair<string, List<string>>(id,row));
                }

                //filling the headers
                for (int i = 1; i <= firstDoc.GetWorksheetStatistics().EndColumnIndex; i++)
                {
                    firstHeader.Add(firstDoc.GetCellValueAsString(1, i));
                }

                for (int i = 1; i <= secondDoc.GetWorksheetStatistics().EndColumnIndex; i++)
                {
                    secondHeader.Add(secondDoc.GetCellValueAsString(1, i));
                }

                // Comparing the elements
                List<String> inFirstOnly = firstList.Except(secondList).ToList();
                List<String> inSecondOnly = secondList.Except(firstList).ToList();
                List<String> common = firstList.Intersect(secondList).ToList();
                
                // The first Document (Common)
                var outputDoc = new SLDocument();

                list = new List<List<string>>();
                list.Add(firstHeader);
                var rowGroups = common.Select((key) => firstDic[key]).ToList();
                foreach (var rowgroup in rowGroups)
                {
                    list.AddRange(rowgroup);
                }
                list.Add(Empty);
                list.Add(secondHeader);
                rowGroups = common.Select((key) => secondDic[key]).ToList();
                foreach (var rowgroup in rowGroups)
                {
                    list.AddRange(rowgroup);
                }

                outputDoc = AddSheet(outputDoc, "Common", list);

                list = new List<List<string>>();
                list.Add(firstHeader);
                rowGroups = inFirstOnly.Select((key) => firstDic[key]).ToList();
                foreach (var rowgroup in rowGroups)
                {
                    list.AddRange(rowgroup);
                }

                outputDoc = AddSheet(outputDoc, "In First Only", list);

                list = new List<List<string>>();
                list.Add(secondHeader);
                rowGroups = inSecondOnly.Select((key) => secondDic[key]).ToList();
                foreach (var rowgroup in rowGroups)
                {
                    list.AddRange(rowgroup);
                }

                outputDoc = AddSheet(outputDoc, "In Second Only", list);
           
                outputDoc.DeleteWorksheet("Sheet1");
                outputDoc.SaveAs(fileName);
                Process.Start(fileName);

            }
            catch (Exception ee)
            {

                MessageBox.Show(ee.Message);
            }
        }

        public SLDocument AddSheet(SLDocument doc, string sheetname, List<List<string>> list)
        {
            doc.AddWorksheet(sheetname);
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    doc.SetCellValue(i + 1, j + 1, list[i][j]);
                }
            }
            return doc;
        }

    }
}

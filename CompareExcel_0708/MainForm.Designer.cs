﻿namespace CompareExcel_0708
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtg_Second = new System.Windows.Forms.DataGridView();
            this.dtg_First = new System.Windows.Forms.DataGridView();
            this.btn_First = new System.Windows.Forms.Button();
            this.btn_Second = new System.Windows.Forms.Button();
            this.btn_Compare = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.cmb_firstSheet = new System.Windows.Forms.ComboBox();
            this.cmb_secondSheet = new System.Windows.Forms.ComboBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_Second)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_First)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtg_Second
            // 
            this.dtg_Second.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_Second.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtg_Second.Location = new System.Drawing.Point(0, 0);
            this.dtg_Second.Name = "dtg_Second";
            this.dtg_Second.Size = new System.Drawing.Size(1012, 191);
            this.dtg_Second.TabIndex = 0;
            // 
            // dtg_First
            // 
            this.dtg_First.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_First.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtg_First.Location = new System.Drawing.Point(0, 0);
            this.dtg_First.Name = "dtg_First";
            this.dtg_First.Size = new System.Drawing.Size(1012, 187);
            this.dtg_First.TabIndex = 1;
            // 
            // btn_First
            // 
            this.btn_First.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_First.Location = new System.Drawing.Point(6, 19);
            this.btn_First.Name = "btn_First";
            this.btn_First.Size = new System.Drawing.Size(99, 23);
            this.btn_First.TabIndex = 2;
            this.btn_First.Text = "Choose First File";
            this.btn_First.UseVisualStyleBackColor = true;
            this.btn_First.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_Second
            // 
            this.btn_Second.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Second.Location = new System.Drawing.Point(229, 17);
            this.btn_Second.Name = "btn_Second";
            this.btn_Second.Size = new System.Drawing.Size(99, 23);
            this.btn_Second.TabIndex = 3;
            this.btn_Second.Text = "Choose Second File";
            this.btn_Second.UseVisualStyleBackColor = true;
            this.btn_Second.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn_Compare
            // 
            this.btn_Compare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Compare.Location = new System.Drawing.Point(916, 11);
            this.btn_Compare.Name = "btn_Compare";
            this.btn_Compare.Size = new System.Drawing.Size(114, 38);
            this.btn_Compare.TabIndex = 4;
            this.btn_Compare.Text = "Compare";
            this.btn_Compare.UseVisualStyleBackColor = true;
            this.btn_Compare.Click += new System.EventHandler(this.btn_Compare_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // cmb_firstSheet
            // 
            this.cmb_firstSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmb_firstSheet.FormattingEnabled = true;
            this.cmb_firstSheet.Location = new System.Drawing.Point(111, 19);
            this.cmb_firstSheet.Name = "cmb_firstSheet";
            this.cmb_firstSheet.Size = new System.Drawing.Size(112, 21);
            this.cmb_firstSheet.TabIndex = 5;
            this.cmb_firstSheet.SelectedIndexChanged += new System.EventHandler(this.cmb_firstSheet_SelectedIndexChanged);
            // 
            // cmb_secondSheet
            // 
            this.cmb_secondSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmb_secondSheet.FormattingEnabled = true;
            this.cmb_secondSheet.Location = new System.Drawing.Point(334, 19);
            this.cmb_secondSheet.Name = "cmb_secondSheet";
            this.cmb_secondSheet.Size = new System.Drawing.Size(112, 21);
            this.cmb_secondSheet.TabIndex = 6;
            this.cmb_secondSheet.SelectedIndexChanged += new System.EventHandler(this.cmb_secondSheet_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmb_secondSheet);
            this.groupBox1.Controls.Add(this.btn_First);
            this.groupBox1.Controls.Add(this.btn_Second);
            this.groupBox1.Controls.Add(this.btn_Compare);
            this.groupBox1.Controls.Add(this.cmb_firstSheet);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 414);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1036, 51);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dtg_Second);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dtg_First);
            this.splitContainer1.Size = new System.Drawing.Size(1012, 382);
            this.splitContainer1.SplitterDistance = 191;
            this.splitContainer1.TabIndex = 10;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 465);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Feedback Checker";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dtg_Second)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_First)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtg_Second;
        private System.Windows.Forms.DataGridView dtg_First;
        private System.Windows.Forms.Button btn_First;
        private System.Windows.Forms.Button btn_Second;
        private System.Windows.Forms.Button btn_Compare;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ComboBox cmb_firstSheet;
        private System.Windows.Forms.ComboBox cmb_secondSheet;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}

